export const landingPageOverviewTitle = "Internet, TV, Phone, Smart Home and Security - Xfinity";
export const mobileTab = "#xc-polaris-menu-mobile-dropdown";
export const mobileOverviewTab = "(//a[@href='https://www.xfinity.com/learn/mobile-service'])[2]"
export const mobileOverviewPageTitle = "Xfinity Mobile: Save on Wireless with Xfinity"